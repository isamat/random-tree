Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :trees, only: [:index,:show] do
  	get 'parent/:id', to: 'trees#show_parents'
  	get 'child/:id', to: 'trees#show_children'
  end
  get 'search', to: 'trees#search'
end
