class TreesController < ApplicationController
	before_action :set_tree, only: [:show, :edit, :update, :destroy]

	def index 
		Tree.delete_all
		n = 10 + rand(20)
		@trees = Tree.efficient_create(n).format_to
		render json: @trees[0].to_json
	end

	def show_parents
		@parents = Tree.find(params[:id]).ancestors
		render json: @parents.ids
	end
	
	def show_children
		@children = Tree.find(params[:id]).descendants
		render json: @children.ids
	end

	def search
		if params[:q]
			@trees = Tree.search(params[:q]).results
			render json: @trees.to_json
		else
			render json: "No records found"
		end
	end

	def show
		render json: @tree.children.ids		
	end

	private

	def set_tree
		@tree = Tree.find(params[:id]) 
	end

end