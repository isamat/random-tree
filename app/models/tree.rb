require 'elasticsearch/model'
class Tree < ApplicationRecord
	include Elasticsearch::Model
	include Elasticsearch::Model::Callbacks
	has_ancestry
	

	def self.format_to
		self.arrange_serializable do |parent, children|
  	{
    	 id: parent.id,
    	 child: children
 		 }
		end	
	end
	def self.efficient_create n 
	 	nodes = []
	 	root = Tree.create!
	 	(n-1).times do 
	 		nodes << Tree.new
	 	end
	  node_ids = Tree.ar_import nodes
	  nodes = Tree.find(node_ids.ids)
		
	  src = []
	  src << root
	  while not nodes.empty? do
			a = src.shuffle[0]
			b = nodes.pop()
			b.parent_id = a.id
			src << b
		end
		Tree.ar_import src, on_duplicate_key_update: {conflict_target: [:id], columns: [:ancestry]}
		
		@nodes = Tree.where(id: node_ids.ids << root.id )
		@nodes.each do |node|
			node.__elasticsearch__.index_document
		end
		@nodes
	end
end