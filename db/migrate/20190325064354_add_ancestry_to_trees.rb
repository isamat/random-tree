class AddAncestryToTrees < ActiveRecord::Migration[5.1]
  def self.up
    add_column :trees, :ancestry, :string
    add_index :trees, :ancestry
  end

  def self.down
    remove_column :trees, :ancestry
    remove_index :trees, :ancestry
  end

end
