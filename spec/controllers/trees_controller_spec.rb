require 'rails_helper'
RSpec.describe TreesController do
  describe "GET #index" do
    before do
    	@tree = FactoryBot.create(:tree)
      get :index
    end
    it "returns http success" do
      expect(response).to have_http_status(:success)
    end
    it "JSON body response contains expected traces" do
      json_response = JSON.parse(response.body)
      expect(response.body).to eq("[[{\"latitude\":\"32.9377784729004\",\"longitude\":\"-117.230392456055\"}]]")
    end
  end
  # describe "post #delete" do
  #   before do
  #   	@coordinate = FactoryBot.create(:coordinate)
  #     delete "/traces/#{@coordinate.trace_id}"
  #     expect(response.status).to eq(204)
  #   end
  # end

  # describe "put #update" do
  #   before do
  #   	@coordinate = FactoryBot.create(:coordinate)
  #   	process :update, method: :put, params: { id: @coordinate.trace_id, "coordinates_attributes" => [{"latitude": 10.9377784729004,"longitude": -10.230392456055}] }
  #   end
  #   it "gets updates" do
  #   	expect(response).to have_http_status(200)
  #   end
  #   it "gets updates have write latitude" do
  #   	@coordinates = @coordinate.trace.coordinates.first
  #   	expect(@coordinates.latitude).to eq(10.9377784729004)
  #   	expect(@coordinates.longitude).to eq(-10.230392456055)
  #   end
  #end
end
