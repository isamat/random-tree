require 'rails_helper'

RSpec.describe Tree, type: :model do
  describe "Creation" do 
    before do 

      @tree = FactoryBot.create(:tree)
      @child = FactoryBot.create(:second_tree, parent: @tree)
    end
    it "can be created" do 
      expect(@tree).to be_valid
    end
    it "it has a child" do 
      expect(@tree.children.first.parent.id).to eq(@tree.id)
    end
    it "it has a parent" do 
      expect(@tree.children.first.parent.id).to eq(@child.parent.id)
    end   
  end
end
